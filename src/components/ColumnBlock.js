import React, { Component } from "react";

import ColumnCircle from "./ColumnCircle";
import ColumnText from "./ColumnText";

class ColumnBlock extends Component {
  render() {
    const texts = this.props.data.secondary_kpis.map((element, index) => {
      return <ColumnText textData={element} key={element.name + index} />;
    });
    return (
      <div className="componentWrapper">
        <ColumnCircle
          name={this.props.data.name}
          circleData={this.props.data.main_kpi}
        />
        <div className="contentBlockWrapper">{texts}</div>
      </div>
    );
  }
}

export default ColumnBlock;
