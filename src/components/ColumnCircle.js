import React, { Component } from "react";

class ColumnCircle extends Component {
  render() {
    return (
      <React.Fragment>
        <p className="titleComponent">{this.props.name}</p>
        <div className="innerCircle">
          <p className="circleName">{this.props.circleData.name}</p>
          <p className="circleValue">{this.props.circleData.value}</p>
          <div className="innerCircleContent">
            <div className="ytd">
              <p>{this.props.circleData.trends.ytl || "--"}</p>
              <p>YTD</p>
            </div>
            <div className="pp">
              <p className="ppValue">
                {this.props.circleData.trends.pp + " %"}
              </p>
              <p>PP</p>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ColumnCircle;
