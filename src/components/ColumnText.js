import React, { Component } from "react";

class ColumnText extends Component {
  render() {
    const { name, value } = this.props.textData;
    return (
      <React.Fragment>
        <div className="textBlockWrapper">
          <p className="textTitle">{name}</p>
          <p className="textValue">{value}</p>
        </div>
      </React.Fragment>
    );
  }
}

export default ColumnText;
