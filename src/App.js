import React, { Component } from "react";
import * as Data from "./data";
import AppMobile from "./AppMobile";
import "./App.css";

import ColumnBlock from "./components/ColumnBlock";

class App extends Component {
  render() {
    const columns = Data.columns.map((element, index) => {
      return <ColumnBlock data={element} key={element.name + index} />;
    });
    return (
      <React.Fragment>
        <div className="App">{columns}</div>
        <div className="AppMobile">
          <AppMobile columns={Data.columns} />
        </div>
      </React.Fragment>
    );
  }
}

export default App;
