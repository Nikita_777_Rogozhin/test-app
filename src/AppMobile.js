import React from "react";
import Slider from "react-slick";

import ColumnBlock from "./components/ColumnBlock";

class AppMobile extends React.Component {
  render() {
    const settings = {
      dots: false,
      arrows: false
    };
    const columns = this.props.columns.map((element, index) => {
      return (
        <div key={element.name + index}>
          <ColumnBlock data={element} />
        </div>
      );
    });
    return (
      <div className="container">
        <Slider {...settings}>{columns}</Slider>
      </div>
    );
  }
}

export default AppMobile;
